# Aiesec Wordpress dev box

## Booting vagrant press and getting the theme repo

1. Clone the repository
2. cd into Vagrant
3. Run vagrant up

cd /vagrant/wordpress/wp-content/themes/

git clone git@bitbucket.org:daviesgo/aiesec-theme.git

## Installing wordpress

Go to localhost:8080 in your browser.

Select English (UK)

Site name: AIESEC
User: admin
Password: vagrant

Login to wordpress

Go to Appearance->Themes

Activate Aiesec

## To ssh into the VM

From within the vagrant directory run vagrant ssh

## MySql

user: root
password: vagrant
host: localhost

## Editing the files

The files are stored on the server in /vagrant/wordpress/

This is the same as the vagrant directory on your local machine.

If you edit a file on your local, it will be visible on the VM.

## Running SASS

Run SASS on your *local* machine.

/vagrant/wordpress/wp-content/themes/*theme_name*/